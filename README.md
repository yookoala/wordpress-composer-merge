# Wordpress with Composer

A simple plugin for manage all composer vendor in the same
folder. This plugin is based on the [composer merge][composer-merge-plugin]
by the [Wikimedia project][wikimedia].

[composer-merge-plugin]: https://packagist.org/packages/wikimedia/composer-merge-plugin
[wikimedia]: https://www.wikimedia.org/


## Install

Install this plugin into your wordpress plugin folder. Remember to activate
the plugin or else the autoloader won't work.


## Usage

Let's say you have 3 different plugins that has a composer.json
config for their dependencies:

```
- wp-contents/
  - plugins/
    - awesome-plugin-1/
      - composer.json
    - awesome-plugin-2/
      - composer.json
    - awesome-plugin-3/
      - composer.json
```

Instead of maintaining 3 different vendor folder, you can install
this plugin like this:

```
- wp-contents/
  - plugins/
    - awesome-plugin-1/
      - composer.json
    - awesome-plugin-2/
      - composer.json
    - awesome-plugin-3/
      - composer.json
    - wordpress-composer-merge/
      - composer.json
```

Then run composer command in the folder, for example:

```
$ cd wp-contents/plugins/wordpress-composer-merge
$ composer install
```

then only maintain 1 vendor folder (and autoloader).

```
- wp-contents/
  - plugins/
    - awesome-plugin-1/
      - composer.json
    - awesome-plugin-2/
      - composer.json
    - awesome-plugin-3/
      - composer.json
    - wordpress-composer-merge/
      - composer.json
      - vendor/
```

`composer install` and `composer update` should work.


## License

This plugin is released under the GPLv2 license. A copy of the license
can be obtain [here][license]

[license]: LICENSE.md
